﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DataVirtualization
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window, INotifyPropertyChanged
    {
        private DemoCustomerProvider customerProvider;
        private DispatcherTimer timer;
        private AsyncVirtualizingCollection<Customer> _customerList;

        public Window1()
        {
            InitializeComponent();
            DataContext = this;
        }

        public AsyncVirtualizingCollection<Customer> CustomerList
        {
            get { return _customerList; }
        }

        private void GetDataHandler(object sender, RoutedEventArgs e)
        {           
            customerProvider = new DemoCustomerProvider(1000 /*number of items*/, 800 /*delay*/);
            _customerList = new AsyncVirtualizingCollection<Customer>(customerProvider, 30 /*page size*/, 3000 /*timeout*/);
            OnPropertyChanged("CustomerList");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
